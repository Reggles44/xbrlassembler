.. XBRLAssembler documentation master file, created by
sphinx-quickstart on Fri May 29 19:36:33 2020.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

.. include:: ../../README.rst

Documentation
=========================================

.. toctree::
   :maxdepth: 2
   :caption: General:

   assembler
   enums

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
