DEL ".pytest_cache" -y
cd tests
DEL "test files" -y
DEL .coverage
DEL test.json
cd ..